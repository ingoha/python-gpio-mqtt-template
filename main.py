from logging.config import fileConfig
from app import DemoApp


if __name__ == '__main__':
    """Main Methode des Programms
    
    Wird ausgeführt, falls alle anderen Skript-Inhalte zuvor abgearbeitet wurden.
    """
    # Konfiguriere die Logausgaben
    fileConfig('./log-config.ini')

    # Erzeuge eine neue Applikation
    demoApp = DemoApp()
    # Verbinde Applikation mit dem MQTT Server
    demoApp.connect_to_mqtt()
    # Starte Applikation
    demoApp.start()
