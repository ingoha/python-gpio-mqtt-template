import datetime
import paho.mqtt.client
import paho.mqtt.client as mqttlib
import logging
from enum import Enum
import gpiod


class LEDS(Enum):
    """Auswahl der zu leuchtenden LED"""
    NONE = 0
    GREEN = 1
    BLUE = 2


class DemoApp:
    """Demo Applikation DHBW Mosbach"""

    # Konfiguration
    # TODO: Ersetzen durch eigene Werte
    # Aufgrund der Konfiguration des mqtt Paketes ohne mqtt://
    # Ändern zu localhost nach Installation von moqsquitto
    MQTT_URL = "test.mosquitto.org"
    MQTT_SEND_TOPIC = "softwareprojekt/mqtt-to-ws"
    MQTT_RECEIVE_TOPIC = "softwareprojekt/ws-to-mqtt"

    # GPIO
    GPIO_PIN_BLUE = 23   # GPIO23
    GPIO_PIN_GREEN = 24  # GPIO24

    # Aktuell gewählte LED, zu Beginn keine
    selected_led = LEDS.NONE

    def __init__(self):
        """Initialisiere Komponente"""
        # Logger
        self.__logger = logging.getLogger('DemoApp')

        self.init_gpio()

        # Neuer MQTT Client
        self.__mqttclient = mqttlib.Client()
        self.__mqttclient.on_connect = self.mqtt_on_connect
        self.__mqttclient.message_callback_add(self.MQTT_RECEIVE_TOPIC, self.on_message_from_ui)

        # Status
        self.__connected = False

        self.select_led(LEDS.NONE)

    def __del__(self):
        """Destruktor"""
        try:
            self.__mqttclient.disconnect()
            del self.__mqttclient
        except AttributeError:
            pass
        del self.__connected
        del self.__logger
        del self.selected_led

    def init_gpio(self):
        # Alternative GPIO.BOARD, dann werden die Pins anders durchnummeriert (von 1 bis ...)
        # BCM nutzt die GPIO-Bezeichnung. GPIO23 -> 23
#        GPIO.setmode(GPIO.BCM)
#        GPIO.setwarnings(False)
        chip = gpiod.Chip('gpiochip0')
        # Ausgänge konfigurieren
        self.__lines = chip.get_lines([self.GPIO_PIN_BLUE, self.GPIO_PIN_GREEN])
        self.__lines.request(consumer='demo', type=gpiod.LINE_REQ_DIR_OUT, default_vals=[0,0])
        #GPIO.setup(self.GPIO_PIN_BLUE, GPIO.OUT)
        #GPIO.setup(self.GPIO_PIN_GREEN, GPIO.OUT)

    def start(self):
        """Starte Applikation"""
        last_sent_second = 0

        # TODO: Durch eigene Logik ersetzen
        while True:
            # Sende Nachricht nur wenn bereits verbunden
            if self.__connected:
                # Sende Serverzeit nur alle 10 Sekunden
                if datetime.datetime.now().second % 10 == 0 and last_sent_second != datetime.datetime.now().second:
                    last_sent_second = datetime.datetime.now().second
                    # Beispiel für das Senden einer Nachricht an die UI
                    self.__mqttclient.publish(self.MQTT_SEND_TOPIC,
                                              f"Serverzeit: {datetime.datetime.now().time()}")

    def connect_to_mqtt(self):
        """Verbinde Applikation mit dem MQTT Server"""
        try:
            self.__mqttclient.connect(self.MQTT_URL)
        except ConnectionError as e:
            self.__logger.error("Es konnte keine Verbindung mit MQTT hergestellt werden.")
            return False
        else:
            # Starten des MQTT Klienten
            self.__mqttclient.loop_start()
            self.__connected = True
            self.__logger.info(f"Verbindung zu MQTT-Server ({self.MQTT_URL}) erfolgreich hergestellt.")

    def select_led(self, new_led: LEDS):
        """Wählt eine neue LED aus und aktualisiert die GPIO Pins"""
        self.selected_led = new_led
        self.__logger.info(f"Neue LED ausgewählt: {LEDS(new_led)}")

        if LEDS.BLUE == new_led:
            self.__lines.set_values([1,0])
        elif LEDS.GREEN == new_led:
            self.__lines.set_values([0,1])
        else:
            self.__lines.set_values([0,0])
        #GPIO.output(self.GPIO_PIN_BLUE, GPIO.HIGH if LEDS.BLUE == new_led else GPIO.LOW)
        #GPIO.output(self.GPIO_PIN_GREEN, GPIO.HIGH if LEDS.GREEN == new_led else GPIO.LOW)

    def on_message_from_ui(self, client, userdata, message: paho.mqtt.client.MQTTMessage):
        """Neue MQTT Nachricht empfangen"""
        # Konvertiere den Inhalt der Nachricht in einen String
        payload = str(message.payload.decode())
        self.__logger.info(f'Neue Nachricht von UI: {payload}')

        # Prüfe ob eine Lampe geändert werden soll
        if payload == "led-none":
            self.select_led(LEDS.NONE)

        if payload == "led-green":
            self.select_led(LEDS.GREEN)

        if payload == "led-blue":
            self.select_led(LEDS.BLUE)

    def mqtt_on_connect(self, client, userdata, flags, rc):
        """MQTT hat erfolgreich eine Verbindung hergestellt."""
        self.__logger.info('MQTT Client erfolgreich verbunden')

        # Auf das Nachrichtenthema hören
        try:
            client.subscribe(self.MQTT_RECEIVE_TOPIC)
        except:
            self.__logger.error("Falsches oder ungültiges MQTT Thema.")
        else:
            self.__logger.info("MQTT Thema {} nun abonniert.".format("softwareprojekt/ws-to-mqtt"))
